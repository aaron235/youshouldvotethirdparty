#!/usr/bin/python3.5

from flask import Flask
from flask import render_template

import argparse


app = Flask( __name__ )


@app.route( '/' )
def index():
	return render_template( 'index.html' )


if __name__ == '__main__':
	parser = argparse.ArgumentParser( description='Specify some configuration options for the webserver' )
	parser.add_argument(
		'--port',
		dest='port',
		action='store',
		default=8080,
		help='Specify the port for the webserver to run on'
		)
	args = parser.parse_args()

	# start the server with proper configuration
	app.debug = True
	app.jinja_env.trim_blocks = True
	app.jinja_env.lstrip_blocks = True

	app.run( host='0.0.0.0', port=int( args.port ) )
